// FORM VALIDATION

// Get variables
const errorMsgEmpty = document.getElementById('empty');
const errorMsgInvalid = document.getElementById('invalid');
const submitBtn = document.getElementById('submitBtn');
const input = document.getElementById('email');
const form = document.getElementById('emailForm');

// Set display property of message paragraphs to none
errorMsgEmpty.style.display = "none";
errorMsgInvalid.style.display = "none";

// Was the form submitted?
// Validate form after the submit button has been clicked
submitBtn.addEventListener('click', function() {
    // Check if the input is empty
    if(input.value == '') {
    // If empty 
        //display error message and add red border to input
        input.classList.add('error');
        errorMsgEmpty.style.display = "block";

        //On focus remove border and error message
        input.addEventListener('focus', function() {
            this.classList.remove('error');
            errorMsgEmpty.style.display = "none";
        });
    } else{
        // If not empty, check email format
            //Is email format valid (name@host.xyz)?
            var valid = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}/.test(input.value);
            
            // If not valid
            if(!valid) {
                // show error message and add red border to input
                input.classList.add('error');
                errorMsgInvalid.style.display = "block";

                //on focus clear input, remove message and border
                input.addEventListener('focus', function() {
                    this.classList.remove('error');
                    this.value = '';
                    errorMsgInvalid.style.display = "none";
                });
            } else {
              // If valid 
                 //submit form and clear input
                 input.value ='';
                 form.submit();
            }
    }     
});


